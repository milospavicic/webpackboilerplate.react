const path = require('path');
require('@babel/polyfill');

module.exports = {
  entry: {
    global: ['./src/bundles/global.jsx'],
    home: ['./src/bundles/home.jsx'],
  },
  output: {
    path: path.join(__dirname, '/build'),
    filename: '[name].bundle.js',
  },
  module: {
    rules: [
      // {
      //   test: path.join(__dirname, './src/bundles/global.jsx'),
      //   use: 'expose-loader?exposes=Global',
      // },
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
        },
      },
    ],
  },
  resolve: {
    modules: [path.resolve('./src/bundles'), 'node_modules'],
    extensions: ['.js', '.jsx'],
  },
};
